FROM docker.elastic.co/logstash/logstash-oss:6.5.1
RUN logstash-plugin install logstash-filter-prune
